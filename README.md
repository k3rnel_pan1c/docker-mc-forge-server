![Repo Logo](icon/icon_256.png)

Docker-MC-Forge
===

## About
This Repo tracks the version of my attempt to build a universal Forge Modpack Server Docker-Image.

**Important!** I make have use of the shell script created by the folks of the ***All the Mods*** project, so I CAN'T and WILL NOT take credit for that script and its config. ALL CREDITS for these 2 files go to their respective creators.

## Tested Compatability

* [All the Mods 3](https://minecraft.curseforge.com/projects/all-the-mods-3)
* [All the Mods 3 - Remix](https://minecraft.curseforge.com/projects/all-the-mods-3-remix)
* more to come, but if you have tested one and it woked for you feel free to create a issue or a pull request to update the readme

## Usage

Should you be interested in using this Image or build your own with the modpack you like here is a quick overview of what you can and NEED to configure.

**TODO:** sorry, I'll deliver this one as soon as possible :S (aka as soon as I have time to write this docu)

## Icon Credits

First of all I am no Artist, I know, but I still felt the need for an icon and so I combined the Minecraft Forge logo and a Docker logo.

Sources for the 2 pngs I used and combined via Incscape (note: no licence applied to either when I checked @ 14-Apr, 2019)
* [Docker Logo](https://iconscout.com/icon/docker-4)
* [Forge Logo](https://pngtree.com/free-icon/forge_856890)
